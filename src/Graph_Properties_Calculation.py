__author__ = 'fz56'

import networkx as nx
import csv
import numpy as np
from graph_tool.all import *
from collections import Counter

graphFileName = "/work/fz56/CTU13NetFlowDataSet/2.csv.graphml"
NodePropertiesName = 'NodeProperties2.csv'

G = nx.read_graphml(graphFileName) # networkx load graph
g = load_graph(graphFileName) # graph-tool load graph
print("load")
Node_Properties_file = open(NodePropertiesName, "w") # write those properties into csv file (table)
NameDic = nx.get_node_attributes(G, "name") #get IP address
localClustering = local_clustering(g) #get clustering coefficient
print("localClustering")
nodeBetweenness, edgeBetweenness = betweenness(g) #get node betweenness centrality
print("Betweenness")
w = g.new_edge_property("double") 
w.a = np.random.random(len(w.a))
ee, x = eigenvector(g, w) #get eigen values

Names = []
ClustBet = []
vprop = g.vertex_properties["name"]
for v in g.vertices(): # combine local clustering coefficient, node betweenness, eigen value by IP address. Those properties are calculated by graph-tool
    Names.append(vprop[v])
    ClustBet.append([localClustering[v], nodeBetweenness[v], x[v]])
print("localClustering-----nodeBetweenness-----eigen values")

for n in NameDic.keys(): # calculate graph properties using networkx and write those properties into a table
    outNeighbors = nx.neighbors(G, n)
    inNeighbors = list(set(nx.all_neighbors(G, n))-set(nx.neighbors(G, n)))
    sumOfInDegreeWeight = 0
    sumOfOutDegreeWeight = 0
    sumOfInDegreeDuration = 0
    sumOfOutDegreeDuration = 0
    sumOfInDegreePort = []
    sumOfOutDegreePort = []
    for nei in outNeighbors:
        for m in G[n][nei].keys():
            sumOfOutDegreeWeight += G[n][nei][m]["TotPkts"]
            sumOfOutDegreeDuration += G[n][nei][m]["Dur"]
            if 'Sport' in G[n][nei][m].keys():
                sumOfOutDegreePort.append(int(G[n][nei][m]["Sport"]))
    if len(sumOfOutDegreePort) > 0:
        countPort = Counter(sumOfOutDegreePort)
        outProtMode = countPort.most_common()[0][0]
    else:
        outProtMode = None
    for nei in inNeighbors:
        for m in G[nei][n].keys():
            sumOfInDegreeWeight += G[nei][n][m]["TotPkts"]
            sumOfInDegreeDuration += G[nei][n][m]["Dur"]
            if 'Dport' in G[nei][n][m].keys():
                sumOfInDegreePort.append(int(G[nei][n][m]["Dport"]))
    if len(sumOfInDegreePort) > 0:
        countPort = Counter(sumOfInDegreePort)
        inProtMode = countPort.most_common()[0][0]
    else:
        inProtMode = None
    ind = Names.index(NameDic[n])
    Node_Properties_file.write(NameDic[n] +\
                               '\t' + str(G.in_degree(n)) +\
                               '\t' + str(G.out_degree(n)) +\
                               '\t' + str(sumOfInDegreeWeight)+\
                               '\t' + str(sumOfOutDegreeWeight)+\
                               '\t' + str(ClustBet[ind][0])+\
                               '\t' + str(ClustBet[ind][1])+\
                               '\t' + str(ClustBet[ind][2])+\
                               '\t' + str(sumOfInDegreeDuration)+\
                               '\t' + str(sumOfOutDegreeDuration)+\
                               '\t' + str(outProtMode)+\
                               '\t' + str(inProtMode)+\
                               '\n')
Node_Properties_file.close()
