__author__ = 'fz56@msstate.edu'
__author__ = 'sh2364@msstate.edu'

import networkx as nx
from random import choice, shuffle, seed
from mpi4py import MPI
import datetime as dt
import ast
from operator import itemgetter

def main(comm=MPI.COMM_WORLD):
    """ This is function to simulate netflow datasets"""
    numprocs = comm.size
    rank = comm.Get_rank()
    severOutDegreePerc = 8.0

    if rank == 0:
        print(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
        print("Number of Processors: ", numprocs)
        print(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
        Graphs = list()
        with open("DatasetsConfig.txt", "r") as myfile:
            lines = myfile.readlines()
            for line in lines:
                Graphs.append(nx.read_graphml(line.strip()))
        inDegrees = list()
        outDegrees = list()
        upperlevelNodesDict = list()
        numOfOriginalGraph = len(Graphs)
        for g in Graphs:
            inDegree = list()
            outDegree = list()
            for n in g.nodes():
                inDegree.append(g.in_degree(n))
                outDegree.append(g.out_degree(n))
            inDegrees.append(inDegree)
            outDegrees.append(outDegree)
        propertyConfigFile = open('PropertyConfig.txt', "w")
        seed(1234)
        properties = list(Graphs[0].get_edge_data(*choice(Graphs[0].edges()))[0].keys())
        for p in properties:
            propertyConfigFile.write(p + '\n')
        propertyConfigFile.close()
        input("Press Enter to continue after configuration in config.txt") # python2 raw_input

        configDic = {}
        configDics = []
        with open("PropertyConfig.txt", "r") as File:
            data = ' '.join([line.replace('\n', '') for line in File.readlines()])
        data = data.split()
        for i in range(0, len(data), 2):
            configDic[data[i]] = ast.literal_eval(data[i+1])
        configDic = dict((key, value) for key, value in configDic.items() if value)
        configDic = dict((key, list()) for key, value in configDic.items())
        for g in Graphs:
            for e in g.edges(data=True):
                for key in configDic.keys():
                    configDic[key].append(e[2][key])
            configDics.append(configDic)
    else:
        configDics = None
        inDegrees = None
        outDegrees = None
        upperlevelNodesDict = None
        numOfOriginalGraph = None
    comm.barrier()
    indegreeList = comm.bcast(inDegrees, root=0)
    outdegreeList = comm.bcast(outDegrees, root=0)
    configDics = comm.bcast(configDics, root=0)
    numOfOriginalGraph = comm.bcast(numOfOriginalGraph, root=0)

    simulatedGraph = nx.MultiDiGraph()
    seed(1234)
    graphIndex = choice(range(numOfOriginalGraph))
    graphIndices = comm.gather(graphIndex, root=0)

    if rank == 0:
        NameLists = []
        sumFlag = 0
        for k in graphIndices:
            NameList = []
            for i in range(len(outdegreeList[k])):
                NameList.append(sumFlag+i)
            sumFlag += len(outdegreeList[k])
            NameLists.append(NameList)
    else:
        NameLists = None
    NameLists = comm.bcast(NameLists, root=0)

    comm.barrier()
    #node = outdegreeList[graphIndex].index(max(outdegreeList[graphIndex]))
    nodes = list()
    threshold_OutDegree = (sum(outdegreeList[graphIndex])*severOutDegreePerc)/100.0
    for i in range(len(outdegreeList[graphIndex])):
        if outdegreeList[graphIndex][i] >= threshold_OutDegree:
            nodes.append(i)

    EdgeList = []
    while sum(outdegreeList[graphIndex]) >= 1:
        outIndex = outdegreeList[graphIndex].index(max(outdegreeList[graphIndex]))
        temp = indegreeList[graphIndex][outIndex]
        indegreeList[graphIndex][outIndex] = 0
        for i in range(outdegreeList[graphIndex][outIndex]):
            maxDegreeIndex = indegreeList[graphIndex].index(max(indegreeList[graphIndex]))
            edgeAttr={}
            for key, value in configDics[graphIndex].items():
                edgeAttr[key] = choice(value)
            EdgeList.append((NameLists[rank][outIndex], NameLists[rank][maxDegreeIndex], edgeAttr))
            indegreeList[graphIndex][maxDegreeIndex] -= 1
        outdegreeList[graphIndex][outIndex] = 0
        indegreeList[graphIndex][outIndex] = temp
    simulatedGraph.add_edges_from(EdgeList)
    nx.write_graphml(simulatedGraph, str(rank)+"_ScaleUP.graphml")

    upperlevelNodesDict = comm.gather({rank: itemgetter(*nodes)(NameLists[rank])}, root=0)
    #upperlevelNodes = comm.gather(NameLists[rank][node], root=0)

    comm.barrier()
    if rank == 0:
        upperlevelNodes = list()
        overallGraph = nx.MultiDiGraph()
        for i in range(len(upperlevelNodesDict)):
            if len(upperlevelNodesDict[i][i])>0:
                upperlevelNodes.extend(upperlevelNodesDict[i][i])
        degree = nx.utils.powerlaw_sequence(len(upperlevelNodes), 2)
        Outdegree = [int(round(e)) for e in degree]
        Indegree= [int(round(e)) for e in degree]
        shuffle(Indegree)
        EdgeList = []
        while sum(Outdegree) >= 1:
            outIndex = Outdegree.index(max(Outdegree))
            temp = Indegree[outIndex]
            Indegree[outIndex] = 0
            for i in range(Outdegree[outIndex]):
                maxDegreeIndex = Indegree.index(max(Indegree))
                edgeAttr={}
                for key, value in configDics[choice(range(numOfOriginalGraph))].items():
                    edgeAttr[key] = choice(value)
                EdgeList.append((upperlevelNodes[outIndex], upperlevelNodes[maxDegreeIndex], edgeAttr))
                Indegree[maxDegreeIndex] -= 1
            Outdegree[outIndex] = 0
            Indegree[outIndex] = temp
        overallGraph.add_edges_from(EdgeList)
        nx.write_graphml(overallGraph, "upperlevelGraph.graphml")
    MPI.Finalize()

if __name__ == "__main__":

    print("---------------------------")
    startTime = dt.datetime.now ()
    #print('begin', startTime)
    main()
    #print('done', dt.datetime.now())
    #print(dt.datetime.now() - startTime)