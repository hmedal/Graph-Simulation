__author__ = 'fz56@msstate.edu'
__author__ = 'sh2364@msstate.edu'

import networkx as nx
from random import choice, shuffle
from mpi4py import MPI
import datetime as dt
import heapq
from operator import itemgetter

def main(fileName, G, comm = MPI.COMM_WORLD):
    numprocs = comm.size
    rank = comm.Get_rank()

    if rank == 0:  # check how many processors to be allocated 
        print(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
        print("Number of Processors: ", numprocs)
        print(" - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
    if rank == 0: # processor 0 read first CTU13 data and get graph properties
    
        inDegree = list()
        outDegree = list()
        pocketsList = list()
        durationList = list()
        sportList = list()
        dportList = list()
        protocolList = list()
        nodes=list()
        for n in G.nodes():
            inDegree.append(G.in_degree(n)) # processor 0 get in degree from the original graph
            outDegree.append(G.out_degree(n)) # processor 0 get out degree from the original graph
        for e in G.edges(data=True):
            pocketsList.append(int(e[2]['TotPkts'])) # processor 0 get packets properties from the original graph
            durationList.append(e[2]['Dur'])  #processor 0 get Duration time properties from the original graph
            sportList.append(int(e[2]['Sport'])) #processor 0 get source port properties from the original graph
            dportList.append(int(e[2]['Dport'])) #processor 0 get Destination port properties from the original graph
            protocolList.append(e[2]['Proto']) #processor 0 get protocol properties from the original graph
        inDegree = inDegree*2   # This is to define in degree distribution of local Graph 
        outDegree = outDegree*2 # This is to define out degree distribution of local Graph 
    else:
        inDegree = None
        outDegree = None
        pocketsList = None
        durationList = None
        sportList = None
        dportList = None
        protocolList = None
        nodes=None
        
    indegreeList = comm.bcast(inDegree, root=0) # broadcast In degree property from processor 0 to all other processors
    outdegreeList = comm.bcast(outDegree, root=0) # broadcast Out degree property from processor 0 to all other processors
    pocketsList = comm.bcast(pocketsList, root=0) # broadcast Packets property from processor 0 to all other processors
    durationList = comm.bcast(durationList, root=0) # broadcast Duration property from processor 0 to all other processors
    sportList = comm.bcast(sportList, root=0) # broadcast source port property from processor 0 to all other processors
    dportList = comm.bcast(dportList, root=0) # broadcast Destination port property from processor 0 to all other processors
    protocolList = comm.bcast(protocolList, root=0) # broadcast protocol property from processor 0 to all other processors
    simulatedGraph = nx.MultiDiGraph()
    NameList = []
    numNodes = 3
    for i in range(len(outdegreeList)): # assign IPs to each processor, here use integer as IPs 
        NameList.append(rank*len(outdegreeList)+i)

    # Take 'numNodes' number of nodes with max outdegree from each local graph, to form enterprise connection
    largest = heapq.nlargest(numNodes, outdegreeList)
    for i in range(len(outdegreeList)):
        if outdegreeList[i] in largest:
            nodes.append(i)
            largest.remove(outdegreeList[i])

    while sum(outdegreeList) >= 1: # do simulation based on degree
        outIndex = outdegreeList.index(max(outdegreeList)) # create edge, the highest out degree node is source node
        temp = indegreeList[outIndex]
        indegreeList[outIndex] = 0
        for i in range(outdegreeList[outIndex]):
            maxDegreeIndex = indegreeList.index(max(indegreeList)) # create edge, the highest in degree node is target node, but different from the source node
            simulatedGraph.add_edge(NameList[outIndex], NameList[maxDegreeIndex], TotPkts=choice(pocketsList), Dur=choice(durationList), Sport=choice(sportList), Dport=choice(dportList), Proto=choice(protocolList))
            indegreeList[maxDegreeIndex] -= 1
        outdegreeList[outIndex] = 0
        indegreeList[outIndex] = temp
    nx.write_graphml(simulatedGraph, str(rank)+".graphml") # write local simulation graph into a file
    upperlevelNodes = comm.gather(itemgetter(*nodes)(NameList), root=0)# processor 0 simulates upper level network from highest out degree nodes of each local network

    if rank == 0:
        overallGraph = nx.MultiDiGraph()
        degree = nx.utils.powerlaw_sequence(len(upperlevelNodes), 2) #upper level network out degree distribution
        Outdegree = [int(round(e)) for e in degree]
        Indegree= [int(round(e)) for e in degree]
        shuffle(Indegree)
        while sum(Outdegree) >= 1: # do upper level network simulation
            outIndex = Outdegree.index(max(Outdegree))
            temp = Indegree[outIndex]
            Indegree[outIndex] = 0
            for i in range(Outdegree[outIndex]):
                maxDegreeIndex = Indegree.index(max(Indegree))
                overallGraph.add_edge(upperlevelNodes[outIndex], upperlevelNodes[maxDegreeIndex], TotPkts=choice(pocketsList), Dur=choice(durationList), Sport=choice(sportList), Dport=choice(dportList), Proto=choice(protocolList))
                Indegree[maxDegreeIndex] -= 1
            Outdegree[outIndex] = 0
            Indegree[outIndex] = temp
        nx.write_graphml(overallGraph, "upperlevelGraph.graphml") # write upper level network into a file
    MPI.Finalize()

if __name__ == "__main__":

    print("---------------------------")
    startTime = dt.datetime.now ()
    print('begin', startTime)
    fileName = "CTU13_5.graphml" # this indicates input graph
    G = nx.read_graphml(fileName) # load read graph into G
    main(fileName,G)
    print('done', dt.datetime.now())
    print(dt.datetime.now() - startTime)