__author__ = 'fz56'
__author__ = 'sh2364'

import networkx as nx
from random import sample, choice
import datetime as dt
import copy

startTime = dt.datetime.now ()
print ('begin', startTime)
simulatedGraph = nx.MultiDiGraph()

graphName = "CTU13_5.graphml"
outputGraphName = "ReplicateGraph_netflow_CTU13_1.graphml"

G = nx.read_graphml(graphName) # load the first CTU13 data
NameDic = nx.get_node_attributes(G, "name") # load its IP address

InDegreeList = list()
OutDegreeList = list()
NameList = list()
Pockets = list()
Sport = list()
Dport = list()
Protocol = list()
Starttime_duration = list()

for e in G.edges(data=True): 
    Pockets.append(int(e[2]['TotPkts'])) # get packets properties from the original graph
    Starttime_duration.append([e[2]['StartTime'],e[2]['Dur']]) #get StartTime and Duration properties from the original graph
    if 'Sport' in e[2]:
        Sport.append(int(e[2]['Sport'])) #get source port properties from the original graph
    else:
        Sport.append(-1)
    if 'Dport' in e[2]:
        Dport.append(int(e[2]['Dport'])) #get Destination port properties from the original graph
    else:
        Dport.append(-1)
    Protocol.append(e[2]['Proto']) #get protocol properties from the original graph

print("load")

tempPockets = Pockets[:]
tempStarttime_duration = copy.deepcopy(Starttime_duration)
tempSport = Sport[:]
tempDport = Dport[:]
tempProtocol = Protocol[:]

for n in NameDic.keys():
    InDegreeList.append(G.in_degree(n)) # get in degree from the original graph
    OutDegreeList.append(G.out_degree(n)) # get out degree from the original graph
    NameList.append(NameDic[n])
simulatedGraph.add_nodes_from(NameList) # add those IPs to simulated graph as graph nodes
tempInDegreeList = InDegreeList[:]
tempOutDegreeList = OutDegreeList[:]

print("Degree created")

while sum(tempOutDegreeList) >= 1: # do simulation based on degree
    outIndex = tempOutDegreeList.index(max(tempOutDegreeList)) # create edge, the highest out degree node is source node
    temp = tempInDegreeList[outIndex]
    tempInDegreeList[outIndex] = 0
    for i in range(tempOutDegreeList[outIndex]):
        maxDegreeIndex = tempInDegreeList.index(max(tempInDegreeList)) # create edge, the highest in degree node is target node, but different from the source node
        stTime_dur = tempStarttime_duration.pop(choice(range(len(tempStarttime_duration))))
        simulatedGraph.add_edge(NameList[outIndex], NameList[maxDegreeIndex], TotPkts=tempPockets.pop(choice(range(len(tempPockets)))), StartTime=stTime_dur[0], Dur=stTime_dur[1], Sport=tempSport.pop(choice(range(len(tempSport)))), Dport=tempDport.pop(choice(range(len(tempDport)))), Proto=tempProtocol.pop(choice(range(len(tempProtocol)))))
        tempInDegreeList[maxDegreeIndex] -= 1
    tempOutDegreeList[outIndex] = 0
    tempInDegreeList[outIndex] = temp

SimulatedPockets = list()
SimulatedStarttime_duration = list()
SimulatedSport = list()
SimulatedDport = list()
SimulatedProtocol = list()

for e in simulatedGraph.edges(data=True):
    SimulatedPockets.append(e[2]['TotPkts'])
    SimulatedStarttime_duration.append([e[2]['StartTime'],e[2]['Dur']])
    SimulatedSport.append(e[2]['Sport'])
    SimulatedDport.append(e[2]['Dport'])
    SimulatedProtocol.append(e[2]['Proto'])


print("---------------Comparison---------------------") # output graph properties of original graph and simulated graph, and make comparison
print("---------------Node---------------------------")
print(G.number_of_nodes())
print(simulatedGraph.number_of_nodes())
print("---------------edges--------------------------")
print(G.number_of_edges())
print(simulatedGraph.number_of_edges())

print("---------------In degree----------------------")
Original_Node_In_Degree = open('Original_Node_In_Degree.txt', "w")
for e in sorted(G.in_degree().values()):
    Original_Node_In_Degree.write(str(e) + ", ")
Original_Node_In_Degree.close()

Simulated_Node_In_Degree = open('Simulated_Node_In_Degree.txt', "w")
for e in sorted(simulatedGraph.in_degree().values()):
    Simulated_Node_In_Degree.write(str(e) + ", ")
Simulated_Node_In_Degree.close()

print("---------------Out degree---------------------")
Original_Node_Out_Degree = open('Original_Node_Out_Degree.txt', "w")
for e in sorted(G.out_degree().values()):
    Original_Node_Out_Degree.write(str(e) + ", ")
Original_Node_Out_Degree.close()

Simulated_Node_Out_Degree = open('Simulated_Node_Out_Degree.txt', "w")
for e in sorted(simulatedGraph.out_degree().values()):
    Simulated_Node_Out_Degree.write(str(e) + ", ")
Simulated_Node_Out_Degree.close()

print("---------------Pockets---------------------")
Original_Pockets = open('Original_Pockets.txt', "w")
for e in sorted(Pockets):
    Original_Pockets.write(str(e) + ", ")
Original_Pockets.close()

Simulated_Pockets = open('Simulated_Pockets.txt', "w")
for e in sorted(SimulatedPockets):
    Simulated_Pockets.write(str(e) + ", ")
Simulated_Pockets.close()

"""print("---------------Duration---------------------")
Original_Duration = open('Original_Duration.txt', "w")
for e in sorted(Duration):
    Original_Duration.write(str(e) + ", ")
Original_Duration.close()
"""
Simulated_Duration = open('Simulated_Duration.txt', "w")
for e in sorted(SimulatedDuration):
    Simulated_Duration.write(str(e) + ", ")
Simulated_Duration.close()

print("---------------Sport---------------------")
Original_Sport = open('Original_Sport.txt', "w")
for e in sorted(Sport):
    Original_Sport.write(str(e) + ", ")
Original_Sport.close()

Simulated_Sport = open('Simulated_Sport.txt', "w")
for e in sorted(SimulatedSport):
    Simulated_Sport.write(str(e) + ", ")
Simulated_Sport.close()

print("---------------Dport---------------------")
Original_Dport = open('Original_Dport.txt', "w")
for e in sorted(Dport):
    Original_Dport.write(str(e) + ", ")
Original_Dport.close()

Simulated_Dport = open('Simulated_Dport.txt', "w")
for e in sorted(Dport):
    Simulated_Dport.write(str(e) + ", ")
Simulated_Dport.close()

print("---------------Protocol---------------------")
Original_Protocol = open('Original_Protocol.txt', "w")
for e in sorted(Protocol):
    Original_Protocol.write(str(e) + ", ")
Original_Protocol.close()

Simulated_Protocol = open('Simulated_Protocol.txt', "w")
for e in sorted(SimulatedProtocol):
    Simulated_Protocol.write(str(e) + ", ")
Simulated_Protocol.close()

nx.write_graphml(simulatedGraph, outputGraphName) # write the simulated graph into a file
print('done', dt.datetime.now())
print(dt.datetime.now () - startTime)
