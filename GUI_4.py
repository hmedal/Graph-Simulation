from PyQt4 import QtCore, QtGui
import sys, random
from PyQt4.Qt import QFileDialog

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_Form(QtGui.QWidget):
    def __init__(self):
        QtGui.QWidget.__init__(self)
        self.setupUi(self)
        
    def setupUi(self, Form):
        Form.setObjectName(_fromUtf8("Form"))
        Form.resize(339, 407)
        self.horizontalLayout = QtGui.QHBoxLayout(Form)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.gridLayout = QtGui.QGridLayout()
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.label_numberOfServer = QtGui.QLabel(Form)
        self.label_numberOfServer.setObjectName(_fromUtf8("label_numberOfServer"))
        self.gridLayout.addWidget(self.label_numberOfServer, 12, 4, 1, 2)
        self.pushButton_add = QtGui.QPushButton(Form)
        self.pushButton_add.setObjectName(_fromUtf8("pushButton_add"))
        self.gridLayout.addWidget(self.pushButton_add, 2, 4, 1, 1)
        self.lineEdit_seed = QtGui.QLineEdit(Form)
        self.lineEdit_seed.setObjectName(_fromUtf8("lineEdit_seed"))
        self.gridLayout.addWidget(self.lineEdit_seed, 4, 1, 1, 4)
        self.label_seed = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_seed.setFont(font)
        self.label_seed.setObjectName(_fromUtf8("label_seed"))
        self.gridLayout.addWidget(self.label_seed, 3, 1, 1, 1)
        self.pushButton_remove = QtGui.QPushButton(Form)
        self.pushButton_remove.setObjectName(_fromUtf8("pushButton_remove"))
        self.gridLayout.addWidget(self.pushButton_remove, 2, 5, 1, 1)
        self.pushButton_generateSeed = QtGui.QPushButton(Form)
        self.pushButton_generateSeed.setObjectName(_fromUtf8("pushButton_generateSeed"))
        self.gridLayout.addWidget(self.pushButton_generateSeed, 4, 5, 1, 1)
        self.checkBox_startTime = QtGui.QCheckBox(Form)
        self.checkBox_startTime.setObjectName(_fromUtf8("checkBox_startTime"))
        self.gridLayout.addWidget(self.checkBox_startTime, 9, 3, 1, 2)
        self.checkBox_protocol = QtGui.QCheckBox(Form)
        self.checkBox_protocol.setObjectName(_fromUtf8("checkBox_protocol"))
        self.gridLayout.addWidget(self.checkBox_protocol, 10, 1, 1, 1)
        self.checkBox_packets = QtGui.QCheckBox(Form)
        self.checkBox_packets.setObjectName(_fromUtf8("checkBox_packets"))
        self.gridLayout.addWidget(self.checkBox_packets, 11, 1, 1, 1)
        self.checkBox_sPort = QtGui.QCheckBox(Form)
        self.checkBox_sPort.setObjectName(_fromUtf8("checkBox_sPort"))
        self.gridLayout.addWidget(self.checkBox_sPort, 12, 1, 1, 1)
        self.checkBox_duration = QtGui.QCheckBox(Form)
        self.checkBox_duration.setObjectName(_fromUtf8("checkBox_duration"))
        self.gridLayout.addWidget(self.checkBox_duration, 10, 3, 1, 2)
        self.spinBox_numberOfServer = QtGui.QSpinBox(Form)
        self.spinBox_numberOfServer.setMaximum(100)
        self.spinBox_numberOfServer.setProperty("value", 100)
        self.spinBox_numberOfServer.setObjectName(_fromUtf8("spinBox_numberOfServer"))
        self.gridLayout.addWidget(self.spinBox_numberOfServer, 12, 3, 1, 1)
        self.checkBox_outdegree = QtGui.QCheckBox(Form)
        self.checkBox_outdegree.setObjectName(_fromUtf8("checkBox_outdegree"))
        self.gridLayout.addWidget(self.checkBox_outdegree, 9, 1, 1, 1)
        self.checkBox_clusteringCoefficient = QtGui.QCheckBox(Form)
        self.checkBox_clusteringCoefficient.setObjectName(_fromUtf8("checkBox_clusteringCoefficient"))
        self.gridLayout.addWidget(self.checkBox_clusteringCoefficient, 11, 3, 1, 3)
        self.label_properties = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_properties.setFont(font)
        self.label_properties.setObjectName(_fromUtf8("label_properties"))
        self.gridLayout.addWidget(self.label_properties, 7, 1, 1, 1)
        self.checkBox_indegree = QtGui.QCheckBox(Form)
        self.checkBox_indegree.setObjectName(_fromUtf8("checkBox_indegree"))
        self.gridLayout.addWidget(self.checkBox_indegree, 8, 1, 1, 1)
        self.checkBox_dPort = QtGui.QCheckBox(Form)
        self.checkBox_dPort.setObjectName(_fromUtf8("checkBox_dPort"))
        self.gridLayout.addWidget(self.checkBox_dPort, 8, 3, 1, 1)
        self.pushButton_ok = QtGui.QPushButton(Form)
        self.pushButton_ok.setObjectName(_fromUtf8("pushButton_ok"))
        self.gridLayout.addWidget(self.pushButton_ok, 14, 4, 1, 1)
        self.pushButton_cancel = QtGui.QPushButton(Form)
        self.pushButton_cancel.setObjectName(_fromUtf8("pushButton_cancel"))
        self.gridLayout.addWidget(self.pushButton_cancel, 14, 5, 1, 1)
        self.label_datasets = QtGui.QLabel(Form)
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)
        self.label_datasets.setFont(font)
        self.label_datasets.setObjectName(_fromUtf8("label_datasets"))
        self.gridLayout.addWidget(self.label_datasets, 0, 1, 1, 1)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 8, 2, 5, 1)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 13, 1, 1, 5)
        self.listWidget_files = QtGui.QListWidget(Form)
        self.listWidget_files.setObjectName(_fromUtf8("listWidget_files"))
        self.gridLayout.addWidget(self.listWidget_files, 1, 1, 1, 5)
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem2, 5, 1, 1, 5)
        self.horizontalLayout.addLayout(self.gridLayout)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        Form.setWindowTitle(_translate("Form", "LANS", None))
        self.label_numberOfServer.setText(_translate("Form", "Number of Server", None))
        self.pushButton_add.setText(_translate("Form", "Add", None))
        self.label_seed.setText(_translate("Form", "Seed", None))
        self.pushButton_remove.setText(_translate("Form", "Remove", None))
        self.pushButton_generateSeed.setText(_translate("Form", "Randomize", None))
        self.checkBox_startTime.setText(_translate("Form", "Start time", None))
        self.checkBox_protocol.setText(_translate("Form", "Protocol", None))
        self.checkBox_packets.setText(_translate("Form", "Packets", None))
        self.checkBox_sPort.setText(_translate("Form", "Sport", None))
        self.checkBox_duration.setText(_translate("Form", "Duration", None))
        self.checkBox_outdegree.setText(_translate("Form", "Outdegree", None))
        self.checkBox_clusteringCoefficient.setText(_translate("Form", "Clustering coefficient", None))
        self.label_properties.setText(_translate("Form", "Properties", None))
        self.checkBox_indegree.setText(_translate("Form", "Indegree", None))
        self.checkBox_dPort.setText(_translate("Form", "Dport", None))
        self.pushButton_ok.setText(_translate("Form", "Ok", None))
        self.pushButton_cancel.setText(_translate("Form", "Cancel", None))
        self.label_datasets.setText(_translate("Form", "Datasets", None))

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    Form = Ui_Form()
    Form.show()
    
    def add_files(self):
        fileNames = QFileDialog.getOpenFileNames()
        for fileName in fileNames:
            Form.listWidget_files.addItem(fileName)
        
    def remove_files(self):
        listItems = Form.listWidget_files.selectedItems()
        if not listItems: return        
        for item in listItems:
            Form.listWidget_files.takeItem(Form.listWidget_files.row(item))
        
    def randomize_seed(self):
        seed = random.random()
        Form.lineEdit_seed.setText(str(seed))
        
    def submit_arguments(self):
        # Set checkBox values
        if Form.checkBox_indegree.isChecked():
            indegree = "True"
        else:
            indegree = "False"
            
        if Form.checkBox_outdegree.isChecked():
            outdegree = "True"
        else:
            outdegree = "False"
            
        if Form.checkBox_protocol.isChecked():
            protocol = "True"
        else:
            protocol = "False"
            
        if Form.checkBox_packets.isChecked():
            packets = "True"
        else:
            packets = "False"
            
        if Form.checkBox_sPort.isChecked():
            sport = "True"
        else:
            sport = "False"
            
        if Form.checkBox_dPort.isChecked():
            dport = "True"
        else:
            dport = "False"
            
        if Form.checkBox_startTime.isChecked():
            startTime = "True"
        else:
            startTime = "False"
            
        if Form.checkBox_duration.isChecked():
            duration = "True"
        else:
            duration = "False"
            
        if Form.checkBox_clusteringCoefficient.isChecked():
            clusteringCoefficient = "True"
        else:
            clusteringCoefficient = "False"
            
        numberOfServer = Form.spinBox_numberOfServer.value()

        # Write properties to text file
            
        text_file = open("PropertyConfig.txt", "w")
        
        text_file.write("Indegree ")
        text_file.write(indegree)
        text_file.write("\n")
        
        text_file.write("Outdegree ")
        text_file.write(outdegree)
        text_file.write("\n")
        
        text_file.write("Protocol ")
        text_file.write(protocol)
        text_file.write("\n")
        
        text_file.write("Packets ")
        text_file.write(packets)
        text_file.write("\n")
        
        text_file.write("Sport ")
        text_file.write(sport)
        text_file.write("\n")
        
        text_file.write("Dport ")
        text_file.write(dport)
        text_file.write("\n")
        
        text_file.write("StartTime ")
        text_file.write(startTime)
        text_file.write("\n")
        
        text_file.write("Duration ")
        text_file.write(duration)
        text_file.write("\n")
        
        text_file.write("ClusteringCoefficient ")
        text_file.write(clusteringCoefficient)
        text_file.write("\n")
        
        text_file.write("NumberOfServer ")
        text_file.write(str(numberOfServer))
        text_file.write("\n")
        
        if Form.lineEdit_seed.text():
            text_file.write("Seed ")
            text_file.write(Form.lineEdit_seed.text())
        
        text_file.close()
        
        # Write datasets to text file
        
        text_file = open("DatasetsConfig.txt", "w")
        for i in range(Form.listWidget_files.count()):
            text_file.write(Form.listWidget_files.item(i).text())
            text_file.write("\n")
            
        text_file.close()
        
        sys.exit(app.exec_())
    
    Form.pushButton_add.clicked.connect(add_files)
    Form.pushButton_remove.clicked.connect(remove_files)
    Form.pushButton_generateSeed.clicked.connect(randomize_seed)
    Form.pushButton_ok.clicked.connect(submit_arguments)
    Form.pushButton_cancel.clicked.connect(QtCore.QCoreApplication.instance().quit)
        
    sys.exit(app.exec_())