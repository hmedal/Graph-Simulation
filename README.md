# Graph-Simulation

The simulation algorithms and graph properties calculation scripts were developed by the Idaho Bailiff G7 Simulation Group (Sarah Harun, Fangyan Zhang, Sushil Poudel, Song Zhang, Hugh Medal, Linkan Bian, Jonathan Davis) at Mississippi State University (MSU). These scripts are tested on the Shadow II HPCC cluster at MSU by Fangyan Zhang (fz56@msstate.edu). If you have any question about these scripts, please contact Fangyan Zhang.

In these code, we used CTU13 netflow datasets, which are downloaded from 
http://mcfp.weebly.com/the-ctu-13-dataset-a-labeled-dataset-with-botnet-normal-and-background-traffic.html

This dataset was captured by CTU University. It includes real botnet traffic mixed with normal traffic and background traffic.
These scripts are tested on Python 3.X, but python2.7 should work as well because no any features that are exclusive to Python 3.X used in scripts. However, to run on python2.7 it is possible that some minor changes need to be made python grammar format.
One document about how to submit jobs to cluster using PBS is provided (Shadow submission procedure.docx). One PBS example script is also provided (ExamplePBS.pbs). You can write your own PBS script based on your need. 

1)  File: Graph_Simulation_Simulation Algorithm.py

This code is used to simulate a graph with the same number of nodes and edges as the original graph (graph simulation algorithm).  It reads in a graph file with graphml format and exports a simulated graph with the similar graph properties, including in degree distribution, out degree distribution, source port distribution, destination port distribution, duration time distribution, total packets distribution, protocol distribution. 

Software dependency:
Python 3.x, 
Python packages: Mpi4py, NetworkX

2) File: Graph_Simulation_Enterprise_Connection_Algorithm.py

This code is used to construct a large graph based sub-graphs (enterprise connection algorithm). It reads in a graph file with graphml format and exports a simulated graph with the similar graph properties. The simulated graph consists of multiple graph files. 

Software dependency:
Python 3.x
Python packages: Mpi4py, NetworkX

3) File: Graph_Properties_Calculation.py

This code is used to calculate graph properties. It reads in a graph file with graphml format and exports a CSV file containing 12 graph properties, in degree, out degree, sum of ingoing edge weight, sum of outgoing edge weight, clustering coefficient, node betweenness, eigenvalue, sum of ingoing edges duration, sum of outgoing edges duration, ingoing edges protocol mode, outgoing edges protocol mode. Each row of the table shows one node properties. 

Software dependency:
Python 3.x
Python packages: NetworkX, Graph-tool
